import { createRouter, createWebHashHistory } from "vue-router";
import CrearLibros from "../components/CrearLibros.vue";
import ListaLibros from "../components/ListaLibros.vue";
import ActualizarLibros from "../components/ActualizarLibros.vue";

const routes = [
  {
    path: "/crear",
    name: "crearL",
    component: CrearLibros,
  },
  {
    path: "/listar",
    name: "listarL",
    component: ListaLibros,
  },
  {
    path: "/actualizar",
    name: "actualizarL",
    component: ActualizarLibros,
    props: true,
  },

];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
